# Capytale anti-cheat

Capytale anti-cheat.

# Test

```bash
npm install
npm run dev
```

# Build

```bash
npm install
npm run build
```

# How to use

## Import

```javascript
import {
  CapytaleAntiCheat,
  ContentHidingMethod,
  ExitDetectionMethod,
} from "@capytale/capytale-anti-cheat";
```

## Use

```javascript
<CapytaleAntiCheat
// props
>
  Content to protect
</CapytaleAntiCheat>
```

### Mandatory props

#### Related to the activity

- `isDirty` (`boolean`): When `true`, indicates that the activity has not been
  saved by the student: this shows a warning before the link allowing the student
  to leave the activity.
- `startLocked` (`boolean`): Whether the activity should start locked. If set to
  `true`, the activity will have to be unlocked before the student is able to
  see the content. This is intended to be used when the activity has already
  been opened by the student before.

#### Related to the anti-cheat

- `enabled` (`boolean`): Whether the anti-cheat is enabled. If set to `false`,
  the content will be rendered directly.
- `contentHidingMethod` (`enum ContentHidingMethod`): The method used to hide the content. Possible values are `ContentHidingMethod.REMOVE_FROM_DOM` (0) and `ContentHidingMethod.DISPLAY_NONE` (1).
- `exitDetectionMethod` (`enum ExitDetectionMethod`): The method used to detect
  exits. Possible values are `ExitDetectionMethod.BLUR` (0) and
  `ExitDetectionMethod.VISIBILITY_CHANGE` (1).
- `disableExitDetection` (`boolean`): Whether to disable exit detection. If set
  to `true`, the user will be able to exit the activity without being flagged as
  cheating.
- `lockDelayMs` (`number`): The delay (in milliseconds) before the activity is
  locked. This is intended to allow the program to disable the exit detection
  if the focus went to an iframe controlled by the activity, for example.
- `hashedPassword` (`string`): The hashed password of the user (using the Bcrypt
  algorithm).

#### Related to the server

- `dbTouchFunction` (`() => Promise<any>`): The function to call when the user
  is about to start the activity for the first time, intended to set a flag in
  the database to remember that the user has started the activity. This function must return a promise that resolves when the flag has been set. If the promise rejects, the activity will not be rendered.

### Optional props

#### Related to the activity

- `activityTitle` (`string`): The title of the activity.
- `studentName` (`string`): The name of the student.
- `returnUrl` (`string`): The URL to lead the student to when they exit the
  activity.

#### Related to the server

- `dbTouchTimeoutMs` (`number`): The timeout (in milliseconds) for the promise
  returned when `dbTouchFunction` is called (default: 10000). If the promise
  does not resolve before the timeout, the promise will be considered rejected.

#### Related to styling

- `antiCheatClassName` (`string`): The class name of the anti-cheat container.
- `contentClassName` (`string`): The class name of the content container.

// Mandatory parameters
activityTitle={activityTitle}
contentHidingMethod={contentHidingMethod}
dbTouchFunction={dbTouchFunction}
disableExitDetection={disableExitDetection}
enabled={true}
exitDetectionMethod={exitDetectionMethod}
hashedPassword={hashedPassword}
isDirty={isDirty}
lockDelayMs={lockDelayMs}
startLocked={startLocked}
studentName={studentName}
// Optional parameters
antiCheatClassName="anti-cheat"
contentClassName="content"
returnUrl={hasReturnUrl ? returnUrl : undefined}
