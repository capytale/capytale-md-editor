import React from "react";
import ReactMarkdown from "react-markdown";
import remarkDirective from "remark-directive";
import remarkDirectiveRehype from "remark-directive-rehype";
import remarkGfm from "remark-gfm";
import remarkMath from "remark-math";
import rehypeKatex from "rehype-katex";
import rehypeRaw from "rehype-raw";

import { AProps, ImgProps } from "react-html-props";

import styles from "./MDViewer.module.scss";
import CodeViewer from "./CodeViewer";

const Remark = function ({ title = null, children }) {
  return (
    <div className={styles.remark}>
      {title && <div className={styles.remarkTitle}>{title}</div>}
      {children}
    </div>
  );
};

const Style = function ({ children }) {
  return (
    <div className={styles.notAllowed}>
      L'élément <code>style</code> n'est pas autorisé ici.
    </div>
  );
};

const Script = function ({ children }) {
  return (
    <div className={styles.notAllowed}>
      L'élément <code>script</code> n'est pas autorisé ici.
    </div>
  );
};

const CodePreview = function ({ inline, children, ...props }) {
  if (inline) {
    return <code>{children[0]}</code>;
  }
  return <CodeViewer children={children} {...props} />;
};

const Image = function (props: ImgProps) {
  return (
    <img
      {...props}
      style={{
        maxWidth: "100%",
        margin: "auto",
      }}
    />
  );
};

const YouTube = function ({ children, ...props }) {
  if (typeof children !== "string") {
    return (
      <div className={styles.notAllowed}>
        L'élément <code>youtube</code> doit avoir un enfant : l'identifiant de
        la vidéo YouTube. Par exemple : <code>:youtube[dQw4w9WgXcQ]</code>.
      </div>
    );
  }
  return (
    <span
      style={{
        maxWidth: "100%",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <iframe
        width="560"
        height="315"
        src={"https://www.youtube-nocookie.com/embed/" + children}
        title="YouTube video player"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        referrerPolicy="strict-origin-when-cross-origin"
        allowFullScreen
      ></iframe>
    </span>
  );
};

const A = function (props: AProps) {
  return <a {...props} target="_blank" rel="noopener noreferrer" />;
};

export default function MdViewer({ source, allowHtml = false }) {
  return (
    // https://github.com/IGassmann/remark-directive-rehype
    <ReactMarkdown
      children={source}
      className={styles.renderedMarkdown}
      remarkPlugins={[
        remarkDirective,
        remarkDirectiveRehype,
        remarkGfm,
        remarkMath,
      ]}
      rehypePlugins={allowHtml ? [rehypeRaw, rehypeKatex] : [rehypeKatex]}
      components={{
        //@ts-ignore
        remark: Remark,
        style: Style,
        script: Script,
        code: CodePreview,
        a: A,
        youtube: YouTube,
        img: Image,
      }}
    />
  );
}
