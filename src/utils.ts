export function appendClassnames(
  ...classNames: (string | null | undefined)[]
): string {
  return classNames.filter((c) => c).join(" ");
}
