import React, { useEffect } from "react";
import PythonCodeEditor from "./code-editors/PythonCodeEditor";
import HTMLCodeEditor from "./code-editors/HTMLCodeEditor";
import CSSCodeEditor from "./code-editors/CSSCodeEditor";
import JSCodeEditor from "./code-editors/JSCodeEditor";
import MDCodeEditor from "./code-editors/MDCodeEditor";
import CodeEditor from "./code-editors/CodeEditor";

import styles from "./CodeViewer.module.scss";
import { useMemo } from "react";

const languageToEditor = {
  "language-python": PythonCodeEditor,
  "language-html": HTMLCodeEditor,
  "language-css": CSSCodeEditor,
  "language-js": JSCodeEditor,
  "language-md": MDCodeEditor,
};

type CodeViewerProps = {
  className?: keyof typeof languageToEditor;
  children: string;
};

export default function CodeViewer({ className, children }: CodeViewerProps) {
  const Editor = useMemo(
    () => languageToEditor[className] ?? CodeEditor,
    [className]
  );
  useEffect(() => {
    console.log(children);
    console.log(typeof children);
  }, [children]);
  return (
    <div className={styles.codePreview}>
      <Editor readOnly={true} code={children ? children.trim() : ""} />
    </div>
  );
}
