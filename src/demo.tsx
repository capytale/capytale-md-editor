import React, { useState } from "react";
import ReactDOM from "react-dom/client";
import { CapytaleHybridMarkdownEditor } from ".";

import { Button } from "react-bootstrap";

import "./demo.css";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

function App() {
  const [code, setCode] = useState("");

  return (
    <div className="app">
      <CapytaleHybridMarkdownEditor code={code} onChange={setCode} />
    </div>
  );
}
