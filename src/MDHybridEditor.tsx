import React, { useState } from "react";
import MDViewer from "./MDViewer";
import MDCodeEditor from "./code-editors/MDCodeEditor";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTableColumns } from "@fortawesome/free-solid-svg-icons/faTableColumns";

import styles from "./MDHybridEditor.module.scss";
import { faXmark } from "@fortawesome/free-solid-svg-icons/faXmark";
import { faExpand } from "@fortawesome/free-solid-svg-icons/faExpand";

type MDHybridEditorProps = {
  code: string;
  allowHtml?: boolean;
  onChange: (code: string) => void;
};

enum PreviewVisibility {
  Fullscreen = "fullscreen",
  Split = "split",
  Hidden = "hidden",
}

export default function MDHybridEditor({
  code,
  allowHtml,
  onChange,
}: MDHybridEditorProps) {
  const [previewVisibility, setPreviewVisibility] = useState<PreviewVisibility>(
    PreviewVisibility.Split
  );
  return (
    <div
      className={styles.rootDiv}
      data-preview-visibility={previewVisibility.toString()}
    >
      {previewVisibility !== PreviewVisibility.Fullscreen && (
        <MDCodeEditor
          code={code}
          onChange={onChange}
          className={styles.editor}
          lineWrapping={true}
        />
      )}
      {previewVisibility === PreviewVisibility.Hidden && (
        <div className={styles.divider}>
          <button
            className={styles.roundedButton}
            onClick={() => setPreviewVisibility(PreviewVisibility.Split)}
          >
            <FontAwesomeIcon icon={faTableColumns} className="previous" />
          </button>
          <button
            className={styles.roundedButton}
            onClick={() => setPreviewVisibility(PreviewVisibility.Fullscreen)}
          >
            <FontAwesomeIcon icon={faExpand} className="previous" />
          </button>
        </div>
      )}
      {previewVisibility !== PreviewVisibility.Hidden && (
        <div className={styles.viewer}>
          <div className={styles.viewerTitle}>
            Résultat
            <div>
              {previewVisibility === PreviewVisibility.Split && (
                <button
                  className={styles.roundedButton}
                  onClick={() =>
                    setPreviewVisibility(PreviewVisibility.Fullscreen)
                  }
                >
                  <FontAwesomeIcon icon={faExpand} className="previous" />
                </button>
              )}
              {previewVisibility === PreviewVisibility.Fullscreen && (
                <button
                  className={styles.roundedButton}
                  onClick={() => setPreviewVisibility(PreviewVisibility.Split)}
                >
                  <FontAwesomeIcon icon={faTableColumns} className="previous" />
                </button>
              )}

              <button
                className={styles.roundedButton}
                onClick={() => setPreviewVisibility(PreviewVisibility.Hidden)}
              >
                <FontAwesomeIcon icon={faXmark} className="previous" />
              </button>
            </div>
          </div>
          <div className={styles.viewerContent}>
            <MDViewer source={code} allowHtml={!!allowHtml} />
          </div>
        </div>
      )}
    </div>
  );
}
